package Server;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


public class Server {
    private final int PORT_NUMBER = 4999;
    private ServerSocket serverSocket;
    private Socket socket;
    private PrintWriter outToClient;
    private BufferedReader inFromClient;
    private InputStreamReader streamReader;
    private ObjectMapper objectMapper = new ObjectMapper();
    private JsonNode jsonNode = objectMapper.createObjectNode();
    private ServerCommands serverCommands;
    static final String SERVER_VERSION = "0.2.0";
    static final String SERVER_CREATION_DATE = "26.01.2024";

    public Server() {
        this.serverCommands = new ServerCommands(this);
    }



    public static void main(String[] args) throws IOException {
        Server server = new Server();
        server.connectionWithClient(server.PORT_NUMBER);
        server.communicationWithClient();
    }

    private void connectionWithClient(int portNumber) throws IOException {
        serverSocket = new ServerSocket(portNumber);
        socket = serverSocket.accept();
        System.out.println("Established connection with Client");
    }

    private void communicationWithClient() throws IOException {
        outToClient = new PrintWriter(socket.getOutputStream(), true);
        streamReader = new InputStreamReader(socket.getInputStream());
        inFromClient = new BufferedReader(streamReader);

        serverCommands.serverMenu(inFromClient, outToClient);

    }

    public void disconnect() throws IOException {
        serverSocket.close();
        socket.close();
        System.out.println("Server has been disconnected");
    }





}

