package Server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Duration;
import java.time.Instant;


public class ServerCommands {
    private Server server;
    private Instant serverStart = Instant.now();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private ObjectNode serverInterface = objectMapper.createObjectNode();
    private ObjectNode helpNode = objectMapper.createObjectNode();
    private ObjectNode serverInfoNode = objectMapper.createObjectNode();
    private ObjectNode uptimeCommand = objectMapper.createObjectNode();
    private ObjectNode unknownCommand = objectMapper.createObjectNode();

    public ServerCommands(Server server) {
        this.server = server;
    }

    public String serverInterface() throws JsonProcessingException {
        serverInterface.put("help", "Shows available commands to use.");
        serverInterface.put("info", "Shows server version and it's creation time.");
        serverInterface.put("uptime", "Shows how long server is running");
        serverInterface.put("stop", "Stops both client and server");

        return objectMapper.writeValueAsString(serverInterface);
    }

    public void serverMenu(BufferedReader inFromClient, PrintWriter outToClient) throws IOException {
        outToClient.println(serverInterface());
        while (true) {
            String command = inFromClient.readLine().toLowerCase();
            switch (command) {
                case "help" -> outToClient.println(getHelpCommand());
                case "info" -> outToClient.println(getInfoCommand());
                case "uptime" -> outToClient.println(getUptimeCommand());
                case "stop" -> {
                    stopCommand(inFromClient, outToClient);
                    return;
                }
                default -> outToClient.println(getUnknownCommand());
            }
        }
    }

    private String getHelpCommand() throws JsonProcessingException {
        helpNode.put("help", "Shows available commands to use.");
        helpNode.put("info", "Shows server version and it's creation time.");
        helpNode.put("uptime", "Shows how long server is running");
        helpNode.put("stop", "Stops both client and server");
        return objectMapper.writeValueAsString(helpNode);
    }
    private String getInfoCommand() throws JsonProcessingException {
        serverInfoNode.put("server version:", Server.SERVER_VERSION);
        serverInfoNode.put("server creation date", Server.SERVER_CREATION_DATE);
        return objectMapper.writeValueAsString(serverInfoNode);
    }
    private String getUptimeCommand() throws JsonProcessingException {
        Instant timeWhenCommandIsUsed = Instant.now();
        Duration serverUptime = Duration.between(serverStart, timeWhenCommandIsUsed);
        long hours = serverUptime.toHours();
        long minutes = serverUptime.toMinutes();
        long seconds = serverUptime.toSeconds();
        uptimeCommand.put("hours", hours);
        uptimeCommand.put("minutes", minutes);
        uptimeCommand.put("seconds", seconds);
        return objectMapper.writeValueAsString(uptimeCommand);
    }
    private void stopCommand(BufferedReader in, PrintWriter out) throws IOException {
        in.close();
        out.close();
        server.disconnect();
    }

    private String getUnknownCommand() throws JsonProcessingException{
        unknownCommand.put("This command", "is unknown");
        return objectMapper.writeValueAsString(unknownCommand);
    }

}
