package Client;

import Server.ServerCommands;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;
import java.util.Scanner;

public class Client {

    private final String IP_ADDRESS = "localhost";
    private final int PORT_NUMBER = 4999;
    private Socket clientSocket;
    private PrintWriter outToServer;
    private BufferedReader inFromServer;
    private InputStreamReader streamReader;
    private final ObjectMapper objectMapper = new ObjectMapper();
    public static void main(String[] args) throws IOException {
        Client client = new Client();
        client.connectionToServer(client.IP_ADDRESS, client.PORT_NUMBER);
        client.communicationWithServer();
    }

    private void connectionToServer (String ipAddress, int port_number) throws IOException {
        clientSocket = new Socket(ipAddress, port_number);
        System.out.println("Successfully established connection with the server.");
    }

    private void communicationWithServer () throws IOException {
        outToServer = new PrintWriter(clientSocket.getOutputStream(), true);
        streamReader = new InputStreamReader(clientSocket.getInputStream());
        inFromServer = new BufferedReader(streamReader);
        Scanner scanner = new Scanner(System.in);
        while (true){
            String messageFromServer = inFromServer.readLine();
            if (messageFromServer != null) {
                readMessageFromServer(messageFromServer);
                outToServer.println(scanner.next());
            } else {
                disconnectFromServer();
                return;
            }
        }
    }

        private void readMessageFromServer(String messageFromServer) throws JsonProcessingException {

            TypeReference<Map<String, String>> mapTypeReference = new TypeReference<>() {};
            Map<String, String> mappedMessage = objectMapper.readValue(messageFromServer, mapTypeReference);
            String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(mappedMessage);
            System.out.println(json);

        }

        private void disconnectFromServer() throws IOException {
        inFromServer.close();
        outToServer.close();
        clientSocket.close();
            System.out.println("Disconnected from server");
        }

}
